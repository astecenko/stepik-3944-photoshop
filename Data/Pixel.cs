﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace MyPhotoshop.Data
{
    public struct Pixel
    {
        private double _r;
        private double _g;
        private double _b;

        public double R
        {
            get => _r;
            set => _r = _g = Check(value);
        }

        public double G
        {
            get => _g;
            set => _g = Check(value);
        }

        public double B
        {
            get => _b;
            set => _b = Check(value);
        }

        public Pixel(double r, double g, double b)
        {
            _r = _b = _g = 0;
            R = r;
            G = g;
            B = b;
        }

        public static Pixel operator *(Pixel p1, double c)
        {
            return new Pixel(Trim(p1.R*c), Trim(p1.G*c), Trim(p1.B*c));
        }

        public static Pixel operator *(double c, Pixel p1)
        {
            return p1 * c;
        }

        public static double Trim(double value)
        {
            return value < 0 ? 0 : value > 1 ? 1 : value;
        }

        private double Check(double value)
        {
            if (value < 0 || value > 1)
                throw new ArgumentException(string.Format("Wrong channel value {0} (the value must be between 0 and 1", value));
            return value;
        }
    }
}
