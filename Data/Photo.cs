using System;
using MyPhotoshop.Data;

namespace MyPhotoshop
{
	public class Photo
	{
		public readonly int width;      
        public readonly int height;
        private readonly Pixel[,] data;

        public Photo(int width, int height)
        {
            this.width = width;
            this.height = height;
            data = new Pixel[width, height];
        }

        public Pixel this[int x, int y]
        {
            get
            {
                CheckPixel(x, y);
                return data[x, y];
            }

            set
            {
                CheckPixel(x, y);
                data[x, y] = value;
            }
        }

        private void CheckPixel(int w, int h)
        {
            if (w > width || h > height) throw new ArgumentException();
        }
	}
}

